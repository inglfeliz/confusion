import React, {Component} from 'react';
import {Media} from 'reactstrap';
import { Card, CardImg, CardImgOverlay, CardText, CardBody,
    CardTitle } from 'reactstrap';
    import DetailsDish from './DetailsDish';
class Menu extends Component{
constructor(props) {
    console.log("constructor es invocado");
    super(props);
    this.state = {
        selectedDish: null
    }
};
onDishSelect(dish) {
    this.setState({ selectedDish: dish});
}
componentDidMount(){
    console.log("Componenete finalizado");
}


render() {
    const menu = this.props.dishes.map((dish) => {
        return (
          <div  className="col-12 col-md-5 m-1">
            <Card key={dish.id}
              onClick={() => this.onDishSelect(dish)}>
              <CardImg width="100%" src={dish.image} alt={dish.name} />
              <CardImgOverlay>
                  <CardTitle>{dish.name}</CardTitle>
              </CardImgOverlay>
            </Card>
          </div>
        );
    });
    console.log("Dibujo el Dom del Componenete")

    return (
        <div className="container">
            <div className="row">
                {menu}
            </div>
            <div className="row">
              
              details
             <DetailsDish dish={this.state.selectedDish}></DetailsDish>
            </div>
        </div>
    );
}


}
export default Menu;